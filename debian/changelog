ruby-eim-xml (1.0.0-1) unstable; urgency=medium

  * d/watch, d/control, d/copyright: use github as upstream
  * New upstream version 1.0.0
  * Bump Standards-Version to 4.7.0 (no changes needed)
  * Ignore ruby3.1's test
  * Drop patches, apply upstream
  * Update d/changelog
  * Enable autopkgtests

 -- Youhei SASAKI <uwabami@gfd-dennou.org>  Thu, 06 Feb 2025 10:29:40 +0900

ruby-eim-xml (0.0.4-6) unstable; urgency=medium

  * Team upload.

  [ HIGUCHI Daisuke (VDR dai) ]
  * d/changelog: fix bug number.
  * d/control: fix salsa url.
  * Refresh packaging using dh-make-ruby.
  * mark Multi-Arch: foreign.

 -- HIGUCHI Daisuke (VDR dai) <dai@debian.org>  Sat, 07 Jan 2023 15:22:41 +0900

ruby-eim-xml (0.0.4-5) unstable; urgency=medium

  * Team upload.

  [ HIGUCHI Daisuke (VDR dai) ]
  * fix FTBFS with ruby-rspec-3.12 (Closes: #1027071).
  * d/control: Bump debhelper-compat Version 13.
  * d/control: Drop XS-Ruby-Version and XB-Ruby-Version.
  * d/control: swap Maintainer and Uploaders.
  * d/control: eliminate lintian warning: ruby-interpreter-is-deprecated.
  * d/control: Bump Standard Version 4.6.1.

  [ Cédric Boutillier ]
  * Team upload.
  * Remove version in the gem2deb build-dependency
  * Bump Standards-Version to 3.9.7 (no changes needed)
  * Run wrap-and-sort on packaging files
  * Add .gitattributes to keep unwanted files out of the source package

  [ Utkarsh Gupta ]
  * Add salsa-ci.yml
  * Update d/ch

  [ Youhei SASAKI ]
  * d/{control,compat}: use debhelper-compat
  * d/control: update Add-{Git,Browser}. use salsa
  * Update d/watch: use gemwatch
  * d/control: Bump Standard Version 4.5.0
  * d/copyright: fix insecure-copyright-format-uri
  * add patch: use Integer instead of Fixnum(deprecated)
  * Add d/control: Add Rules-Requires-Root
  * Disable rdoc generation for repro. build

 -- HIGUCHI Daisuke (VDR dai) <dai@debian.org>  Mon, 02 Jan 2023 14:07:56 +0900

ruby-eim-xml (0.0.4-4) unstable; urgency=medium

  [ Cédric Boutillier ]
  * debian/control: remove obsolete DM-Upload-Allowed flag
  * debian/copyright: use DEP5 copyright-format/1.0

  [ Youhei SASAKI ]
  * Refresh patch: Upgrade RSpec >= 3
  * Bump Standard version: 3.9.6
  * Use libjs-jquery for RDoc generated documents

 -- Youhei SASAKI <uwabami@gfd-dennou.org>  Mon, 28 Sep 2015 17:24:22 +0900

ruby-eim-xml (0.0.4-3) unstable; urgency=low

  * Update watch file

 -- Youhei SASAKI <uwabami@gfd-dennou.org>  Tue, 24 Jan 2012 02:22:06 +0900

ruby-eim-xml (0.0.4-2) unstable; urgency=low

  * Add Vcs-Git: move pkg-ruby-extras Git repo.
  * Add ruby-tests.rake

 -- Youhei SASAKI <uwabami@gfd-dennou.org>  Tue, 24 Jan 2012 01:43:55 +0900

ruby-eim-xml (0.0.4-1) unstable; urgency=low

  * Initial release (Closes: #635339)

 -- Youhei SASAKI <uwabami@gfd-dennou.org>  Wed, 20 Jul 2011 04:56:58 +0900
